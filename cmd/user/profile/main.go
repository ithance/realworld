package main

import (
	"RealWorld/internal/pkg/observability"
	"RealWorld/internal/pkg/users"
	"RealWorld/internal/pkg/utils"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

func ProfileHandler(ctx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	traceConfig := observability.XrayTraceConfiguration{}
	xe, _ := traceConfig.SetConfiguration(ctx)
	defer xe.Flush()
	logrus.WithFields(logrus.Fields{
		"Context": ctx,
		"Event":   event,
	}).Info("Retrieving User Profile")
	response, err := users.GetProfile(ctx, event)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"Context": ctx,
			"Event":   event,
			"error":   err.Error(),
		}).Fatal("Failed to Get User Profile")
	} else {
		logrus.WithFields(logrus.Fields{
			"Context":  ctx,
			"Event":    event,
			"response": response,
		}).Info("Created User Profile Response")
	}
	return response, nil
}

func main() {
	lambda.Start(ProfileHandler)
}
