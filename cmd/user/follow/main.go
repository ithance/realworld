package main

import (
	"RealWorld/internal/pkg/observability"
	"RealWorld/internal/pkg/users"
	"RealWorld/internal/pkg/utils"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

func FollowHandler(ctx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	traceConfig := observability.XrayTraceConfiguration{}
	xe, _ := traceConfig.SetConfiguration(ctx)
	defer xe.Flush()
	logrus.WithFields(logrus.Fields{
		"Context": ctx,
		"Event":   event,
	}).Info("Following User")
	response, err := users.Follow(ctx, event)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"Context": ctx,
			"Event":   event,
			"error":   err.Error(),
		}).Fatal("Failed to Follow User")
	} else {
		logrus.WithFields(logrus.Fields{
			"Context":  ctx,
			"Event":    event,
			"response": response,
		}).Info("Created Follow User Response")
	}
	return response, nil
}

func main() {
	lambda.Start(FollowHandler)
}
