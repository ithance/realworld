package main

import (
	"RealWorld/internal/pkg/articles"
	"RealWorld/internal/pkg/observability"
	"RealWorld/internal/pkg/utils"
	"context"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/sirupsen/logrus"
)

func ListHandler(ctx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	traceConfig := observability.XrayTraceConfiguration{}
	xe, _ := traceConfig.SetConfiguration(ctx)
	defer xe.Flush()
	logrus.WithFields(logrus.Fields{
		"Context": ctx,
		"Event":   event,
	}).Info("Getting Articles")
	response, err := articles.List(ctx, event)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"Context": ctx,
			"Event":   event,
			"error":   err.Error(),
		}).Fatal("Failed to get Articles")
	} else {
		logrus.WithFields(logrus.Fields{
			"Context":  ctx,
			"Event":    event,
			"response": response,
		}).Info("Created Article List Response")
	}
	return response, nil
}

func main() {
	lambda.Start(ListHandler)
}
