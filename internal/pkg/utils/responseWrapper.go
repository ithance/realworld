package utils

import (
	"bytes"
	"context"
	"encoding/json"
	"github.com/aws/aws-lambda-go/events"
	"go.opencensus.io/trace"
)

type Response events.APIGatewayProxyResponse

func Envelope(ctx context.Context, statusCode int, data interface{}) (Response, error) {
	_, span := trace.StartSpan(ctx, "ResponseWrapper.envelope")
	defer span.End()
	var buf bytes.Buffer
	var body []byte
	var err error
	_ = err
	if data != nil {
		if statusCode < 300 {
			body, err = json.Marshal(data)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				return Response{StatusCode: 404}, err
			}
		} else {
			body, err = json.Marshal(map[string]map[string]string{
				"errors": {
					"body": data.(string),
				},
			})
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				return Response{StatusCode: 404}, err
			}
		}
		json.HTMLEscape(&buf, body)
	}

	resp := Response{
		StatusCode:      statusCode,
		IsBase64Encoded: false,
		Body:            buf.String(),
		Headers: map[string]string{
			"Content-Type":                     "application/json; charset=utf-8",
			"Access-Control-Allow-Origin":      "*",
			"Access-Control-Allow-Credentials": "true",
		},
	}
	span.SetStatus(trace.Status{
		Code:    trace.StatusCodeOK,
		Message: "Successfully Built Response",
	})
	span.Annotate([]trace.Attribute{
		trace.StringAttribute("Response_body", string(body)),
		trace.Int64Attribute("Status_Code", int64(statusCode)),
	}, "Failed to get get Table Name")
	return resp, nil
}
