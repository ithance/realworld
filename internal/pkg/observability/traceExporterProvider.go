package observability

import (
	"context"
	xray "contrib.go.opencensus.io/exporter/aws"
	log "github.com/sirupsen/logrus"
	"go.opencensus.io/trace"
)

type TraceExporterProvider interface {
	getExporter() (trace.Exporter, error)
}

type XrayTracer struct {
	ctx context.Context
}

func (xrayexporter *XrayTracer) GetExporter() (*xray.Exporter, error) {
	exporter, err := xray.NewExporter(
		xray.WithVersion("latest"),
		xray.WithOrigin(xray.OriginECS),
		xray.WithOnExport(func(in xray.OnExport) {
			log.WithFields(log.Fields{
				"TraceID": in.TraceID,
				"Context": xrayexporter.ctx,
			}).Info("Sending to Xray")
		}),
	)
	if err != nil {
		log.WithField("Context", xrayexporter.ctx).Fatal("Was unable to initialise the Xray Exporter")
		return nil, err
	}
	return exporter, err
}
