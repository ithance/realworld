package observability

import (
	"context"
	xray "contrib.go.opencensus.io/exporter/aws"
	"github.com/sirupsen/logrus"
	"go.opencensus.io/trace"
)

type TraceConfiguration interface {
	setConfiguration(ctx context.Context)
}

type XrayTraceConfiguration struct {
	ctx context.Context
}

func (xray *XrayTraceConfiguration) SetConfiguration(ctx context.Context) (*xray.Exporter, error) {
	provider := XrayTracer{ctx: ctx}
	exporter, err := provider.GetExporter()
	if err != nil {
		logrus.WithField("Context", ctx).Fatal("Could not configure Tracer")
		return nil, err
	} else {
		trace.RegisterExporter(exporter)
		trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})
		logrus.WithFields(logrus.Fields{
			"Context": ctx,
			"Tracer":  exporter,
		}).Info("Successfully Setup Xray Tracer")
		return exporter, nil
	}
}
