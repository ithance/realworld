package dbclient

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"go.opencensus.io/trace"
	"os"
)

func GetDynamoDBClient(ctx context.Context) (*dynamodb.DynamoDB, error) {
	_, span := trace.StartSpan(ctx, "DynamoDB.create")
	defer span.End()
	var sess *session.Session
	var err error
	if os.Getenv("IS_OFFLINE") != "" {
		config := aws.NewConfig().WithRegion("localhost").WithEndpoint("http://localhost:8000")
		sess, err = session.NewSession(config)
	} else {
		sess, err = session.NewSession()
	}
	client := dynamodb.New(sess)
	sessConfigJson, _ := json.Marshal(sess.Config)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeUnknown,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("SessionConfig", string(sessConfigJson)),
		}, "Failed to create DynamoDB Client")
	} else {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeOK,
			Message: "Successfully created DynamoDB Client",
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("SessionConfig", string(sessConfigJson)),
		}, "Created DynamoDb Client")
	}
	return client, err
}

func GetTableName(ctx context.Context, name string) (string, error) {
	_, span := trace.StartSpan(ctx, "DynamoDB.getTableName")
	defer span.End()
	if os.Getenv("DYNAMODB_NAMESPACE") == "" {
		err := os.Setenv("DYNAMODB_NAMESPACE", "dev")
		if err != nil {
			span.SetStatus(trace.Status{
				Code:    trace.StatusCodeUnavailable,
				Message: err.Error(),
			})
			span.Annotate([]trace.Attribute{
				trace.StringAttribute("DYNAMODB_NAMESPACE", os.Getenv("DYNAMODB_NAMESPACE")),
				trace.StringAttribute("SearchName", name),
			}, "Failed to get get Table Name")
		}
		return "", err
	} else {
		tableName := fmt.Sprintf("realworld-%s-%s", os.Getenv("DYNAMODB_NAMESPACE"), name)
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeOK,
			Message: "Successfully Read DYNAMODB_NAMESPACE",
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("DYNAMODB_NAMESPACE", os.Getenv("DYNAMODB_NAMESPACE")),
			trace.StringAttribute("SearchName", name),
			trace.StringAttribute("TableName", tableName),
		}, "Received Table Name")
		return tableName, nil
	}
}
