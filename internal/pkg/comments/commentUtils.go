package comments

import (
	"RealWorld/internal/pkg/articles"
	"RealWorld/internal/pkg/users"
	"context"
	"encoding/json"
)

// Convert Comment Struct into Json
func commentMarshaller(comment Comment) string {
	commentBytes, err := json.Marshal(comment)
	if err != nil {
		return ""
	} else {
		return string(commentBytes)
	}
}

func transformRetrievedComment(ctx context.Context, article articles.Article, comment Comment, authenticatedUser *users.User) (*CommentResponse, error) {
	if authenticatedUser != nil {
		article.Favorited = contains(article.FavoritedBy, authenticatedUser.Username)
	} else {
		article.Favorited = false
	}

	reader := users.ProfileReader{}
	authorProfile, err := reader.GetProfileByUsername(ctx, article.Author, authenticatedUser)
	if err != nil {
		return nil, err
	}
	commentResponse := CommentResponse{
		Id:        comment.Id,
		Slug:      comment.Slug,
		CreatedAt: comment.CreatedAt,
		UpdatedAt: comment.UpdatedAt,
		Body:      comment.Body,
		Author:    *authorProfile,
	}
	return &commentResponse, nil
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
