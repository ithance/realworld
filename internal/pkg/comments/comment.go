package comments

import (
	"RealWorld/internal/pkg/users"
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"time"
)

type Comment struct {
	Id        string    `json:"id,omitempty" dynamodbay:"id"`
	Slug      string    `json:"slug,omitempty" dynamodbav:"slug"`
	Body      string    `json:"body" dynamodbav:"body"`
	CreatedAt time.Time `json:"createdAt,omitempty" dynamodbav:"createdAt,omitempty,unixtime"`
	UpdatedAt time.Time `json:"updatedAt,omitempty" dynamodbav:"updatedAt,omitempty,unixtime"`
	Author    string    `json:"author,omitempty" dynamodbav:"author"`
}

type CommentResponse struct {
	Id        string        `json:"id"`
	Slug      string        `json:"slug"`
	Body      string        `json:"body"`
	CreatedAt time.Time     `json:"createdAt"`
	UpdatedAt time.Time     `json:"updatedAt"`
	Author    users.Profile `json:"author"`
}

type MultipleCommentResponses struct {
	Comments []CommentResponse `json:"comments"`
}

type commentDBClient interface {
	writeComment(comment Comment) error
	readComment(id string) (*Comment, error)
	readComments(slug string) ([]Comment, error)
	removeComment(id string) error
}

type dynamoDBCommentClient struct {
	dynamodb *dynamodb.DynamoDB
	ctx      context.Context
}

type commentReader struct {
	client commentDBClient
}

type commentWriter struct {
	client commentDBClient
}
