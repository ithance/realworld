package comments

import (
	"RealWorld/internal/pkg/dbclient"
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/sirupsen/logrus"
	"go.opencensus.io/trace"
)

func (writer *commentWriter) saveComment(ctx context.Context, comment Comment) error {
	writer.client = &dynamoDBCommentClient{nil, ctx}
	err := writer.client.writeComment(comment)
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (writer *commentWriter) deleteComment(ctx context.Context, commentId string) error {
	writer.client = &dynamoDBCommentClient{nil, ctx}
	err := writer.client.removeComment(commentId)
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (reader *commentReader) getComment(ctx context.Context, commentId string) (*Comment, error) {
	reader.client = &dynamoDBCommentClient{nil, ctx}
	comment, err := reader.client.readComment(commentId)
	if err != nil {
		return nil, err
	} else {
		return comment, nil
	}
}

func (reader *commentReader) getComments(ctx context.Context, slug string) ([]Comment, error) {
	reader.client = &dynamoDBCommentClient{nil, ctx}
	comments, err := reader.client.readComments(slug)
	if err != nil {
		return nil, err
	} else {
		return comments, nil
	}
}

func (client *dynamoDBCommentClient) writeComment(comment Comment) error {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.WriteComment")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return err
	} else {
		commentsTable, err := dbclient.GetTableName(ctx, "comments")
		if err != nil {
			return err
		} else {
			putItemInput := dynamodb.PutItemInput{}
			item, _ := dynamodbattribute.MarshalMap(comment)
			putItemInput = *putItemInput.SetTableName(commentsTable).SetItem(item)
			_, err := client.dynamodb.PutItem(&putItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Put_Item_Input", putItemInput.String()),
					trace.StringAttribute("Comment", commentMarshaller(comment)),
					trace.StringAttribute("Comment_Table", commentsTable),
				}, "Failed to Write Comment")
				return err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Wrote Comment",
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Put_Item_Input", putItemInput.String()),
					trace.StringAttribute("Comment", commentMarshaller(comment)),
					trace.StringAttribute("Comment_Table", commentsTable),
				}, "Successfully Wrote Comment")
				return nil
			}
		}
	}
}

func (client *dynamoDBCommentClient) readComments(slug string) ([]Comment, error) {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.WriteComment")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return nil, err
	} else {
		commentsTable, err := dbclient.GetTableName(ctx, "comments")
		if err != nil {
			return nil, err
		} else {
			queryInput := dynamodb.QueryInput{}
			queryInput = *queryInput.SetTableName(commentsTable).SetIndexName("article").SetKeyConditionExpression("slug = :slug").SetExpressionAttributeValues(map[string]*dynamodb.AttributeValue{
				":slug": {
					S: aws.String(slug),
				},
			})
			results, err := client.dynamodb.Query(&queryInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeNotFound,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", queryInput.String()),
					trace.StringAttribute("Comments_Table", commentsTable),
				}, "Failed to find Comments For Article")
				logrus.WithFields(logrus.Fields{
					"Query":          queryInput.String(),
					"Query_Error":    err.Error(),
					"Comments_Table": commentsTable,
				}).Info("Failed to find Comments for article")
				return nil, err
			}
			comments := []Comment{}
			err = dynamodbattribute.UnmarshalListOfMaps(results.Items, &comments)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", queryInput.String()),
					trace.StringAttribute("Comments_Table", commentsTable),
				}, "Failed to find Comments For Article")
				return nil, err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Found Comments for Article!",
				})
				return comments, nil
			}
		}
	}
}

func (client *dynamoDBCommentClient) readComment(id string) (*Comment, error) {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.WriteComment")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return nil, err
	} else {
		commentsTable, err := dbclient.GetTableName(ctx, "comments")
		if err != nil {
			return nil, err
		} else {
			getItemInput := dynamodb.GetItemInput{}
			getItemInput = *getItemInput.SetTableName(commentsTable).SetKey(map[string]*dynamodb.AttributeValue{
				"id": {
					S: aws.String(id),
				},
			})
			result, err := client.dynamodb.GetItem(&getItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeNotFound,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("Comment_Id", id),
					trace.StringAttribute("Comments_Table", commentsTable),
				}, "Failed to find Comment")
				return nil, err
			}
			comment := Comment{}
			err = dynamodbattribute.UnmarshalMap(result.Item, &comment)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("Comment_Id", id),
					trace.StringAttribute("Comments_Table", commentsTable),
				}, "Failed to find Comment")
				return nil, err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Found Comment!",
				})

				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("Comment_Id", id),
					trace.StringAttribute("Comments_Table", commentsTable),
					trace.StringAttribute("Comment", commentMarshaller(comment)),
				}, "Found Comment!")
				return &comment, nil
			}
		}
	}
}

func (client *dynamoDBCommentClient) removeComment(id string) error {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.WriteComment")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return err
	} else {
		commentsTable, err := dbclient.GetTableName(ctx, "comments")
		if err != nil {
			return err
		} else {
			deleteItemInput := dynamodb.DeleteItemInput{}
			deleteItemInput = *deleteItemInput.SetTableName(commentsTable).SetKey(map[string]*dynamodb.AttributeValue{
				"id": {
					S: aws.String(id),
				},
			})
			_, err := client.dynamodb.DeleteItem(&deleteItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Delete_Item_Input", deleteItemInput.String()),
					trace.StringAttribute("Comment_Id", id),
					trace.StringAttribute("Comments_Table", commentsTable),
				}, "Failed to Delete Comment")
				return err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Deleted Article",
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Delete_Item_Input", deleteItemInput.String()),
					trace.StringAttribute("Comment_Id", id),
					trace.StringAttribute("Comments_Table", commentsTable),
				}, "Successfully Deleted Comment")
				return nil
			}
		}
	}
}
