package comments

import (
	"RealWorld/internal/pkg/articles"
	"RealWorld/internal/pkg/users"
	"RealWorld/internal/pkg/utils"
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/tidwall/gjson"
	"go.opencensus.io/trace"
	"strings"
	"time"
)

func Create(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Comment.Create")
	defer span.End()
	authenticatedUser, err := users.AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Must be logged in")
	}

	commentResult := gjson.Get(event.Body, "comment")

	if !commentResult.Exists() {
		return utils.Envelope(ctx, 422, "Comment must be specified.")
	}

	logrus.WithField("CommentResult", commentResult.String()).Info("Got Comment")
	commentString := strings.Replace(commentResult.String(), `\`, "", -1)
	logrus.WithField("unescapedString", commentString).Info("Got Comment")
	comment := Comment{}
	_ = json.Unmarshal([]byte(commentString), &comment)

	if comment.Body == "" {
		return utils.Envelope(ctx, 422, "Comment must be specified.")
	}

	slug, ok := event.PathParameters["slug"]

	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing slug parameter",
		})
		return utils.Envelope(ctx, 422, "slug must be specified")
	}

	articleReader := articles.ArticleReader{}
	article, err := articleReader.GetArticle(ctx, slug)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article not found: [%s]", slug))
	}
	timestamp := time.Now()
	comment.Id = uuid.New().String()
	comment.Slug = slug
	comment.CreatedAt = timestamp
	comment.UpdatedAt = timestamp
	comment.Author = authenticatedUser.Username

	writer := commentWriter{}
	err = writer.saveComment(ctx, comment)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Comment", commentMarshaller(comment)),
		}, "Failed to Save Comment")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"comment": comment,
		}).Fatal("Failed to Create Comment")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	response, err := transformRetrievedComment(ctx, *article, comment, authenticatedUser)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Comment", commentMarshaller(comment)),
		}, "Failed to Save Comment")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
			"comment": comment,
		}).Fatal("Failed to Create Comment for Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 201, map[string]CommentResponse{
		"comment": *response,
	})
}

func Get(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Comment.Get")
	defer span.End()

	authenticatedUser, _ := users.AuthenticateAndGetUser(ctx, event)

	slug, ok := event.PathParameters["slug"]

	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing slug parameter",
		})
		return utils.Envelope(ctx, 422, "slug must be specified")
	}

	articleReader := articles.ArticleReader{}
	article, err := articleReader.GetArticle(ctx, slug)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article not found: [%s]", slug))
	}

	reader := commentReader{}
	comments, err := reader.getComments(ctx, slug)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Comments not found for Article: [%s]", slug))
	}

	var commentResponses []CommentResponse

	for _, comment := range comments {
		commentResponse, err := transformRetrievedComment(ctx, *article, comment, authenticatedUser)
		if err != nil {
			continue
		}
		commentResponses = append(commentResponses, *commentResponse)
	}

	return utils.Envelope(ctx, 200, MultipleCommentResponses{
		Comments: commentResponses,
	})

}

func Delete(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Comment.Delete")
	defer span.End()

	authenticatedUser, err := users.AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Must be logged in")
	}

	commentId, ok := event.PathParameters["id"]

	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing slug parameter",
		})
		return utils.Envelope(ctx, 422, "Comment Id must be specified")
	}

	reader := commentReader{}
	comment, err := reader.getComment(ctx, commentId)

	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Comment not found: [%s]", commentId))
	}

	if comment.Author != authenticatedUser.Username {
		return utils.Envelope(ctx, 422, fmt.Sprintf("COmment can only be deleted by author: [%s]", comment.Author))
	}

	writer := commentWriter{}
	err = writer.deleteComment(ctx, commentId)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Comment", commentMarshaller(*comment)),
		}, "Failed to Delete Comment")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"comment": comment,
		}).Fatal("Failed to Delete COmment")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 204, nil)
}
