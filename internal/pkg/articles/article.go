package articles

import (
	"RealWorld/internal/pkg/users"
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"time"
)

type Article struct {
	Slug           string    `json:"slug,omitempty" dynamodbav:"slug"`
	Title          string    `json:"title,omitempty" dynamodbav:"title"`
	Description    string    `json:"description,omitempty" dynamodbav:"description"`
	Body           string    `json:"body,omitempty" dynamodbav:"body"`
	TagList        []string  `json:"tagList,omitempty" dynamodbav:"tagList,omitemptyelem,stringset"`
	CreatedAt      time.Time `json:"createdAt,omitempty" dynamodbav:"createdAt,omitempty,unixtime"`
	UpdatedAt      time.Time `json:"updatedAt,omitempty" dynamodbav:"updatedAt,omitempty,unixtime"`
	Favorited      bool      `json:"favorited,omitempty" dynamodbav:"favorited"`
	FavoritedBy    []string  `json:"-" dynamodbav:"favoritedBy,omitemptyelem,stringset"`
	FavoritesCount int64     `json:"favoritesCount,omitempty" dynamodbav:"favoritesCount"`
	Author         string    `json:"author,omitempty" dynamodbav:"author"`
}

type ArticleResponse struct {
	Slug           string        `json:"slug"`
	Title          string        `json:"title"`
	Description    string        `json:"description"`
	Body           string        `json:"body"`
	TagList        []string      `json:"tagList"`
	CreatedAt      time.Time     `json:"createdAt"`
	UpdatedAt      time.Time     `json:"updatedAt"`
	Favorited      bool          `json:"favorited"`
	FavoritesCount int64         `json:"favoritesCount"`
	Author         users.Profile `json:"author"`
}

type MultipleArticlesResponse struct {
	Articles      []ArticleResponse `json:"articles"`
	ArticlesCount int               `json:"articlesCount"`
}

type articleDBClient interface {
	writeArticle(article Article) error
	findArticle(slug string) (*Article, error)
	removeArticle(slug string) error
	findArticles(author string, tag string, favorited string, followed []string, limit int64, offset int64) ([]Article, error)
	fetchTags() ([]string, error)
}

type dynamoDBArticleClient struct {
	dynamodb *dynamodb.DynamoDB
	ctx      context.Context
}

type ArticleReader struct {
	client articleDBClient
}

type ArticleWriter struct {
	client articleDBClient
}
