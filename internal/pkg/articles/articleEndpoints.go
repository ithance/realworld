package articles

import (
	"RealWorld/internal/pkg/users"
	"RealWorld/internal/pkg/utils"
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/gosimple/slug"
	"github.com/sirupsen/logrus"
	"github.com/tidwall/gjson"
	"go.opencensus.io/trace"
	"math"
	"math/rand"
	"sort"
	"strconv"
	"strings"
	"time"
)

func Create(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.Create")
	defer span.End()
	authenticatedUser, err := users.AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Must be logged in")
	}

	articleResult := gjson.Get(event.Body, "article")

	if !articleResult.Exists() {
		return utils.Envelope(ctx, 422, "Article must be specified.")
	}

	logrus.WithField("ArticleResult", articleResult.String()).Info("Got Article")
	articleString := strings.Replace(articleResult.String(), `\`, "", -1)
	logrus.WithField("unescapedString", articleString).Info("Got Article")
	article := Article{}
	_ = json.Unmarshal([]byte(articleString), &article)

	if article.Title == "" {
		return utils.Envelope(ctx, 422, "title must be specified.")
	}

	if article.Description == "" {
		return utils.Envelope(ctx, 422, "description must be specified.")
	}

	if article.Body == "" {
		return utils.Envelope(ctx, 422, "body must be specified")
	}

	timestamp := time.Now()
	var articleslug strings.Builder
	articleslug.WriteString(slug.Make(article.Title))
	articleslug.WriteString("-")
	articleslug.WriteString(strconv.FormatInt(int64(rand.Float64()*math.Pow(36.0, 6.0)), 10))
	article.Slug = articleslug.String()
	article.CreatedAt = timestamp
	article.UpdatedAt = timestamp
	article.Author = authenticatedUser.Username

	writer := ArticleWriter{}
	err = writer.saveArticle(ctx, article)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(article)),
		}, "Failed to Save Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Create Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	response, err := transformRetrievedArticle(ctx, article, authenticatedUser)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(article)),
		}, "Failed to Save Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Create Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 201, map[string]ArticleResponse{
		"article": *response,
	})
}

func Get(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.Get")
	defer span.End()

	authenticatedUser, _ := users.AuthenticateAndGetUser(ctx, event)

	slug, ok := event.PathParameters["slug"]

	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing slug parameter",
		})
		return utils.Envelope(ctx, 422, "slug must be specified")
	}
	reader := ArticleReader{}
	article, err := reader.GetArticle(ctx, slug)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article not found: [%s]", slug))
	}
	response, err := transformRetrievedArticle(ctx, *article, authenticatedUser)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(*article)),
		}, "Failed to Get Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Get Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 201, map[string]ArticleResponse{
		"article": *response,
	})
}

func Update(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.Update")
	defer span.End()

	authenticatedUser, err := users.AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Must be logged in")
	}

	slug, ok := event.PathParameters["slug"]

	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing slug parameter",
		})
		return utils.Envelope(ctx, 422, "slug must be specified")
	}

	articleResult := gjson.Get(event.Body, "article")

	if !articleResult.Exists() {
		return utils.Envelope(ctx, 422, "Article must be specified.")
	}

	logrus.WithField("ArticleResult", articleResult.String()).Info("Got Article")
	articleString := strings.Replace(articleResult.String(), `\`, "", -1)
	logrus.WithField("unescapedString", articleString).Info("Got Article")
	articleChanges := Article{}
	_ = json.Unmarshal([]byte(articleString), &articleChanges)

	if articleChanges.Title == "" && articleChanges.Description == "" && articleChanges.Body == "" {
		return utils.Envelope(ctx, 422, "At least one field must be specified: [title, description, articleChanges].")
	}

	reader := ArticleReader{}
	article, err := reader.GetArticle(ctx, slug)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article not found: [%s]", slug))
	}

	if article.Author != authenticatedUser.Username {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article can only be updated by author: [%s]", article.Author))
	}

	if articleChanges.Title != "" {
		article.Title = articleChanges.Title
	}

	if articleChanges.Body != "" {
		article.Body = articleChanges.Body
	}

	if articleChanges.Description != "" {
		article.Description = articleChanges.Description
	}

	if len(articleChanges.TagList) > 0 {
		article.TagList = articleChanges.TagList
	}
	article.UpdatedAt = time.Now()
	writer := ArticleWriter{}
	err = writer.saveArticle(ctx, *article)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(*article)),
		}, "Failed to Save Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Update Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	response, err := transformRetrievedArticle(ctx, *article, authenticatedUser)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(*article)),
		}, "Failed to Save Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Update Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 201, map[string]ArticleResponse{
		"article": *response,
	})
}

func Delete(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.Delete")
	defer span.End()

	authenticatedUser, err := users.AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Must be logged in")
	}

	slug, ok := event.PathParameters["slug"]

	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing slug parameter",
		})
		return utils.Envelope(ctx, 422, "slug must be specified")
	}

	reader := ArticleReader{}
	article, err := reader.GetArticle(ctx, slug)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article not found: [%s]", slug))
	}

	if article.Author != authenticatedUser.Username {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article can only be deleted by author: [%s]", article.Author))
	}

	writer := ArticleWriter{}
	err = writer.deleteArticle(ctx, article.Slug)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(*article)),
		}, "Failed to Delete Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Delete Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 204, nil)
}

func List(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.List")
	defer span.End()

	authenticatedUser, _ := users.AuthenticateAndGetUser(ctx, event)

	var limit int64
	var offset int64
	var err error
	limitString, ok := event.PathParameters["limit"]

	if ok {
		limit, err = strconv.ParseInt(limitString, 10, 64)
		if err != nil {
			limit = 20
		}
	}

	offsetString, ok := event.PathParameters["offset"]
	if ok {
		offset, _ = strconv.ParseInt(offsetString, 10, 64)
	}
	tag, _ := event.PathParameters["tag"]
	author, _ := event.PathParameters["author"]
	favorited, _ := event.PathParameters["favorited"]

	if (tag != "" && author != "") || (tag != "" && favorited != "") || (author != "" && favorited != "") {
		return utils.Envelope(ctx, 422, "Only one of these can be specified: [tag, author, favorited]")
	}

	span.Annotate([]trace.Attribute{
		trace.StringAttribute("Event_Method", event.HTTPMethod),
		trace.StringAttribute("author", author),
		trace.Int64Attribute("offset", offset),
		trace.Int64Attribute("limit", limit),
	}, "Search Parameters")
	reader := ArticleReader{}

	articles, err := reader.getArticles(ctx, author, tag, favorited, nil, limit, offset)

	if err != nil {
		return utils.Envelope(ctx, 422, "Articles not found")
	}
	articleResponses := []ArticleResponse{}

	for _, article := range articles {
		articleResponse, err := transformRetrievedArticle(ctx, article, authenticatedUser)
		if err != nil {
			continue
		}
		articleResponses = append(articleResponses, *articleResponse)
	}

	return utils.Envelope(ctx, 200, MultipleArticlesResponse{
		Articles:      articleResponses,
		ArticlesCount: len(articleResponses),
	})
}

func GetTags(outerCtx context.Context) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.GetTags")
	defer span.End()

	reader := ArticleReader{}
	tags, err := reader.getTags(ctx)
	if err != nil {
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}

	return utils.Envelope(ctx, 200, map[string][]string{
		"tags": tags,
	})
}

func Favorite(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.Favorite")
	defer span.End()

	authenticatedUser, err := users.AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Must be logged in")
	}

	slug, ok := event.PathParameters["slug"]

	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing slug parameter",
		})
		return utils.Envelope(ctx, 422, "slug must be specified")
	}

	reader := ArticleReader{}
	article, err := reader.GetArticle(ctx, slug)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Article not found: [%s]", slug))
	}

	shouldFollow := event.HTTPMethod != "DELETE"

	if shouldFollow && !contains(article.FavoritedBy, authenticatedUser.Username) {
		article.FavoritedBy = append(article.FavoritedBy, authenticatedUser.Username)
		article.FavoritesCount = int64(len(article.FavoritedBy))
	} else {
		if contains(article.FavoritedBy, authenticatedUser.Username) {
			sort.Strings(article.FavoritedBy)
			index := sort.SearchStrings(article.FavoritedBy, authenticatedUser.Username)
			article.FavoritedBy[index] = article.FavoritedBy[len(article.FavoritedBy)-1] // Copy last element to index
			article.FavoritedBy[len(article.FavoritedBy)-1] = ""                         // Erase last element (write zero value)
			article.FavoritedBy = article.FavoritedBy[:len(article.FavoritedBy)-1]       // Truncate slice
			article.FavoritesCount = int64(len(article.FavoritedBy))
		}
	}
	writer := ArticleWriter{}
	err = writer.saveArticle(ctx, *article)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(*article)),
		}, "Failed to Save Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Update Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	response, err := transformRetrievedArticle(ctx, *article, authenticatedUser)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("Article", articleMarshaller(*article)),
		}, "Failed to Save Article")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"article": article,
		}).Fatal("Failed to Update Article")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 200, map[string]ArticleResponse{
		"article": *response,
	})
}

func Feed(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "Article.List")
	defer span.End()

	authenticatedUser, _ := users.AuthenticateAndGetUser(ctx, event)

	var limit int64
	var offset int64
	var err error
	limitString, ok := event.PathParameters["limit"]

	if ok {
		limit, err = strconv.ParseInt(limitString, 10, 64)
		if err != nil {
			limit = 20
		}
	}

	offsetString, ok := event.PathParameters["offset"]
	if ok {
		offset, _ = strconv.ParseInt(offsetString, 10, 64)
	}

	followed, err := users.GetFollowedUsers(ctx, authenticatedUser.Username)

	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("user", users.UserMarshaller(*authenticatedUser)),
		}, "Failed to favorited Articles")
		logrus.WithFields(logrus.Fields{
			"context": ctx,
			"event":   event,
			"error":   err.Error(),
			"user":    users.UserMarshaller(*authenticatedUser),
		}).Fatal("Failed to favorited Articles")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	span.Annotate([]trace.Attribute{
		trace.StringAttribute("Event_Method", event.HTTPMethod),
		trace.Int64Attribute("offset", offset),
		trace.Int64Attribute("limit", limit),
	}, "Search Parameters")

	reader := ArticleReader{}

	articles, err := reader.getArticles(ctx, "", "", "", followed, limit, offset)

	if err != nil {
		return utils.Envelope(ctx, 422, "Articles not found")
	}
	articleResponses := []ArticleResponse{}

	for _, article := range articles {
		articleResponse, err := transformRetrievedArticle(ctx, article, authenticatedUser)
		if err != nil {
			continue
		}
		articleResponses = append(articleResponses, *articleResponse)
	}

	return utils.Envelope(ctx, 200, MultipleArticlesResponse{
		Articles:      articleResponses,
		ArticlesCount: len(articleResponses),
	})
}
