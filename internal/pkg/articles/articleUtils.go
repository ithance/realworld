package articles

import (
	"RealWorld/internal/pkg/users"
	"context"
	"encoding/json"
	"sort"
)

func unique(stringSlice []string) []string {
	keys := make(map[string]bool)
	list := []string{}
	for _, entry := range stringSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	sort.Strings(list)
	return list
}

// Convert Article Struct into JSON
func articleMarshaller(article Article) string {
	articleBytes, err := json.Marshal(article)
	if err != nil {
		return ""
	} else {
		return string(articleBytes)
	}
}

// Convert Article Struct into ArticleResponse Struct
func transformRetrievedArticle(ctx context.Context, article Article, authenticatedUser *users.User) (*ArticleResponse, error) {
	article.Favorited = contains(article.FavoritedBy, authenticatedUser.Username)
	reader := users.ProfileReader{}
	authorProfile, err := reader.GetProfileByUsername(ctx, article.Author, authenticatedUser)
	if err != nil {
		return nil, err
	}
	articleResponse := ArticleResponse{
		Slug:           article.Slug,
		Title:          article.Title,
		Body:           article.Body,
		Description:    article.Description,
		CreatedAt:      article.CreatedAt,
		UpdatedAt:      article.UpdatedAt,
		Favorited:      article.Favorited,
		FavoritesCount: article.FavoritesCount,
		TagList:        article.TagList,
		Author:         *authorProfile,
	}
	return &articleResponse, nil
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
