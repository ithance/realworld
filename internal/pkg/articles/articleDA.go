package articles

import (
	"RealWorld/internal/pkg/dbclient"
	"context"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/sirupsen/logrus"
	"go.opencensus.io/trace"
	"strconv"
	"strings"
)

func (writer *ArticleWriter) saveArticle(ctx context.Context, article Article) error {
	writer.client = &dynamoDBArticleClient{nil, ctx}
	err := writer.client.writeArticle(article)
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (writer *ArticleWriter) deleteArticle(ctx context.Context, slug string) error {
	writer.client = &dynamoDBArticleClient{nil, ctx}
	err := writer.client.removeArticle(slug)
	if err != nil {
		return err
	} else {
		return nil
	}
}

func (reader *ArticleReader) GetArticle(ctx context.Context, slug string) (*Article, error) {
	reader.client = &dynamoDBArticleClient{nil, ctx}
	article, err := reader.client.findArticle(slug)
	if err != nil {
		return nil, err
	} else {
		return article, nil
	}
}

func (reader *ArticleReader) getArticles(ctx context.Context, author string, tag string, favorited string, followed []string, limit int64, offset int64) ([]Article, error) {
	reader.client = &dynamoDBArticleClient{nil, ctx}
	articles, err := reader.client.findArticles(author, tag, favorited, followed, limit, offset)
	if err != nil {
		return nil, err
	} else {
		return articles, nil
	}
}

func (reader *ArticleReader) getTags(ctx context.Context) ([]string, error) {
	reader.client = &dynamoDBArticleClient{nil, ctx}
	tags, err := reader.client.fetchTags()
	if err != nil {
		return nil, err
	} else {
		return tags, nil
	}
}

func (client *dynamoDBArticleClient) writeArticle(article Article) error {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.WriteArticle")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return err
	} else {
		articlesTable, err := dbclient.GetTableName(ctx, "articles")
		if err != nil {
			return err
		} else {
			putItemInput := dynamodb.PutItemInput{}
			item, _ := dynamodbattribute.MarshalMap(article)
			putItemInput = *putItemInput.SetTableName(articlesTable).SetItem(item)
			_, err := client.dynamodb.PutItem(&putItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Put_Item_Input", putItemInput.String()),
					trace.StringAttribute("Article_Title", article.Title),
					trace.StringAttribute("Article_Table", articlesTable),
				}, "Failed to Write Article")
				return err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Wrote Article",
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Put_Item_Input", putItemInput.String()),
					trace.StringAttribute("Article_Title", article.Title),
					trace.StringAttribute("Article_Table", articlesTable),
				}, "Successfully Wrote Article")
				return nil
			}
		}
	}
}

func (client *dynamoDBArticleClient) findArticle(slug string) (*Article, error) {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.FindArticle")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return nil, err
	} else {
		articlesTable, err := dbclient.GetTableName(ctx, "articles")
		if err != nil {
			return nil, err
		} else {
			getItemInput := dynamodb.GetItemInput{}
			getItemInput = *getItemInput.SetTableName(articlesTable).SetKey(map[string]*dynamodb.AttributeValue{
				"slug": {
					S: aws.String(slug),
				},
			})
			result, err := client.dynamodb.GetItem(&getItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeNotFound,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("Slug", slug),
					trace.StringAttribute("Articles_Tables", articlesTable),
				}, "Failed to find Article")
				return nil, err
			}
			article := Article{}
			err = dynamodbattribute.UnmarshalMap(result.Item, &article)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("Slug", slug),
					trace.StringAttribute("Articles_Tables", articlesTable),
				}, "Failed to find Article")
				return nil, err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Found User!",
				})

				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("Slug", slug),
					trace.StringAttribute("Articles_Tables", articlesTable),
					trace.StringAttribute("Article", articleMarshaller(article)),
				}, "Found User!")
				return &article, nil
			}
		}
	}
}

func (client *dynamoDBArticleClient) removeArticle(slug string) error {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.RemoveArticle")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return err
	} else {
		articlesTable, err := dbclient.GetTableName(ctx, "articles")
		if err != nil {
			return err
		} else {
			deleteItemInput := dynamodb.DeleteItemInput{}
			deleteItemInput = *deleteItemInput.SetTableName(articlesTable).SetKey(map[string]*dynamodb.AttributeValue{
				"slug": {
					S: aws.String(slug),
				},
			})
			_, err := client.dynamodb.DeleteItem(&deleteItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Delete_Item_Input", deleteItemInput.String()),
					trace.StringAttribute("Slug", slug),
					trace.StringAttribute("Articles_Tables", articlesTable),
				}, "Failed to Delete Article")
				return err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Deleted Article",
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Delete_Item_Input", deleteItemInput.String()),
					trace.StringAttribute("Slug", slug),
					trace.StringAttribute("Articles_Tables", articlesTable),
				}, "Successfully Deleted Article")
				return nil
			}
		}
	}
}

func (client *dynamoDBArticleClient) findArticles(author string, tag string, favorited string, followed []string, limit int64, offset int64) ([]Article, error) {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.FindArticles")
	defer span.End()
	span.Annotate([]trace.Attribute{
		trace.StringAttribute("Author", author),
		trace.StringAttribute("Tag", tag),
		trace.StringAttribute("Favorited", favorited),
		trace.Int64Attribute("Limit", limit),
		trace.Int64Attribute("Offest", offset),
	}, "Variables to process")
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return nil, err
	} else {
		articlesTable, err := dbclient.GetTableName(ctx, "articles")
		if err != nil {
			return nil, err
		} else {
			var lastKey map[string]*dynamodb.AttributeValue
			var queryResults dynamodb.QueryOutput
			queryInput := dynamodb.QueryInput{}
			for i := int64(0); i < offset; i++ {
				//reset on every loop
				queryInput := dynamodb.QueryInput{}
				queryInput = *queryInput.SetTableName(articlesTable).SetIndexName(
					"updatedAt").SetScanIndexForward(false).SetLimit(limit)

				if lastKey != nil {
					queryInput = *queryInput.SetExclusiveStartKey(lastKey)
				}

				if tag != "" {
					queryInput = *queryInput.SetFilterExpression("contains(tagList, :tag)").SetExpressionAttributeValues(map[string]*dynamodb.AttributeValue{
						":tag": {
							S: aws.String(tag),
						},
					})
				}

				if author != "" {
					queryInput = *queryInput.SetFilterExpression("author = :author").SetExpressionAttributeValues(map[string]*dynamodb.AttributeValue{
						":author": {
							S: aws.String(author),
						},
					})
				}

				if favorited != "" {
					queryInput = *queryInput.SetFilterExpression("contains(favoritedBy, :favorited)").SetExpressionAttributeValues(map[string]*dynamodb.AttributeValue{
						":favorited": {
							S: aws.String(favorited),
						},
					})
				}

				if followed != nil {
					queryInput = *queryInput.SetFilterExpression("author IN")
					for index, author := range followed {
						authorIndex := strings.Builder{}
						authorIndex.WriteString(":author")
						authorIndex.WriteString(strconv.FormatInt(int64(index), 10))
						queryInput = *queryInput.SetExpressionAttributeValues(map[string]*dynamodb.AttributeValue{
							authorIndex.String(): {
								S: aws.String(author),
							},
						})
					}
				}

				result, err := client.dynamodb.Query(&queryInput)
				if err != nil {
					span.SetStatus(trace.Status{
						Code:    trace.StatusCodeNotFound,
						Message: err.Error(),
					})
					span.Annotate([]trace.Attribute{
						trace.StringAttribute("Query", queryInput.String()),
						trace.StringAttribute("Articles_Table", articlesTable),
					}, "Failed to find Articles")
					logrus.WithFields(logrus.Fields{
						"Query":          queryInput.String(),
						"Query_Error":    err.Error(),
						"Articles_Table": articlesTable,
					}).Info("Failed to find Articles")
					return nil, err
				}
				if result.LastEvaluatedKey != nil {
					lastKey = result.LastEvaluatedKey
				}
				queryResults = *result
			}
			articles := []Article{}
			err := dynamodbattribute.UnmarshalListOfMaps(queryResults.Items, &articles)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", queryInput.String()),
					trace.StringAttribute("Articles_Tables", articlesTable),
				}, "Failed to find Articles")
				return nil, err
			} else {
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", queryInput.String()),
					trace.StringAttribute("Articles_Table", articlesTable),
				}, "Failed to find Articles")
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Found Articles!",
				})
				return articles, nil
			}
		}
	}
}

func (client *dynamoDBArticleClient) fetchTags() ([]string, error) {
	ctx, span := trace.StartSpan(client.ctx, "DBClient.FetchTags")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	tags := []string{}
	if err != nil {
		return nil, err
	} else {
		articlesTable, err := dbclient.GetTableName(ctx, "articles")
		if err != nil {
			return nil, err
		} else {
			var lastKey map[string]*dynamodb.AttributeValue
			scanInput := dynamodb.ScanInput{}
			attributesToGet := "attributesToGet"
			for {
				scanInput = *scanInput.SetTableName(articlesTable).SetAttributesToGet([]*string{&attributesToGet})
				if lastKey != nil {
					scanInput = *scanInput.SetExclusiveStartKey(lastKey)
				}
				result, err := client.dynamodb.Scan(&scanInput)
				if err != nil {
					span.SetStatus(trace.Status{
						Code:    trace.StatusCodeNotFound,
						Message: err.Error(),
					})
					span.Annotate([]trace.Attribute{
						trace.StringAttribute("Scan", scanInput.String()),
						trace.StringAttribute("Articles_Tables", articlesTable),
					}, "Failed to find Tags")
					logrus.WithFields(logrus.Fields{
						"Scan":           scanInput.String(),
						"Query_Error":    err.Error(),
						"Articles_Table": articlesTable,
					}).Info("Failed to find Tags")
					return nil, err
				}
				if result.LastEvaluatedKey != nil {
					lastKey = result.LastEvaluatedKey
				}
				items := []string{}
				// Unmarshal the Items field in the result value to the Item Go type.
				err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &items)
				if err != nil {
					span.SetStatus(trace.Status{
						Code:    trace.StatusCodeUnknown,
						Message: err.Error(),
					})
					span.Annotate([]trace.Attribute{
						trace.StringAttribute("Scan", scanInput.String()),
						trace.StringAttribute("Articles_Tables", articlesTable),
					}, "Failed to UnMarshal TagList")
					return nil, err
				} else {
					for _, item := range items {
						tags = append(tags, item)
					}
				}
				if lastKey == nil {
					break
				}
			}
		}
	}
	return unique(tags), nil
}
