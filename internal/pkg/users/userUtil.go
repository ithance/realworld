package users

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"go.opencensus.io/trace"
	"golang.org/x/crypto/bcrypt"
	"strings"
	"time"
)

//Converts User struct into a json String
func UserMarshaller(user User) string {
	userBytes, err := json.Marshal(user)
	if err != nil {
		return ""
	} else {
		return string(userBytes)
	}
}

//Converts Profile struct into a json String
func ProfileMarshaller(profile Profile) string {
	profileBytes, err := json.Marshal(profile)
	if err != nil {
		return ""
	} else {
		return string(profileBytes)
	}
}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func checkPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

//Creates a new JWT Token for a user
func mintUserToken(outerCtx context.Context, username string) (*string, error) {
	_, span := trace.StartSpan(outerCtx, "User.CreateToken")
	defer span.End()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, UserClaims{
		username,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Unix() + int64(4.32e+7), //Two Days
			IssuedAt:  time.Now().Unix(),
			Issuer:    "RealWorldAPI",
		},
	})
	tokenString, err := token.SignedString([]byte("[%CI]>53r4kk^kqu+VmL`VEMf%U5e4sx$+Yu%Og!NxJV7U?FI-V]QfeO8JJg2AV")) //Yes hard coding this was bad, but I was tired at the time. Use SSM Parameter Store
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeUnknown,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("User_Name", username),
		}, "Failed to Create Token!")
		return nil, err
	} else {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeOK,
			Message: "Created Token",
		})

		span.Annotate([]trace.Attribute{
			trace.StringAttribute("User_Name", username),
			trace.StringAttribute("Token", tokenString),
		}, "Successfully Created Token")
		return &tokenString, nil
	}
}

func getTokenFromEvent(ctx context.Context, event events.APIGatewayProxyRequest) (*string, error) {
	_, span := trace.StartSpan(ctx, "User.GetToken")
	defer span.End()
	tokenString, err := event.Headers["Authorization"]
	if !err {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeUnauthenticated,
			Message: "Request was sent without Auth Token",
		})
		return nil, errors.New("token missing")
	} else {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeOK,
			Message: "Received Token",
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Token", tokenString),
		}, "Successfully Received Token")
	}
	bearToken := strings.Replace(tokenString, "Token ", "", 1)
	logrus.WithField("Token", bearToken).Info("Received Token")
	return &bearToken, nil
}

func AuthenticateAndGetUser(outerCtx context.Context, event events.APIGatewayProxyRequest) (*User, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.Authenticate")
	defer span.End()
	tokenString, err := getTokenFromEvent(ctx, event)
	if err != nil {
		return nil, err
	} else {
		token, err := jwt.ParseWithClaims(*tokenString, &UserClaims{}, func(token *jwt.Token) (interface{}, error) {
			return []byte("[%CI]>53r4kk^kqu+VmL`VEMf%U5e4sx$+Yu%Og!NxJV7U?FI-V]QfeO8JJg2AV"), nil //I couldn't even be bothered to make it global constant. Please don't do this.
		})
		if claims, ok := token.Claims.(*UserClaims); ok && token.Valid {
			username := claims.Username
			reader := UserReader{}
			authenticatedUser, err := reader.getByUsername(ctx, username)
			if err != nil {
				logrus.WithFields(logrus.Fields{
					"Username": username,
					"user":     authenticatedUser,
					"claims":   claims,
				}).Fatal("Failed to Authenticate user")
				return nil, err
			} else {
				return authenticatedUser, nil
			}
		} else {
			span.SetStatus(trace.Status{
				Code:    trace.StatusCodePermissionDenied,
				Message: err.Error(),
			})
			return nil, err
		}
	}
}
