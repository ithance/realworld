package users

import (
	"context"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/dgrijalva/jwt-go"
)

type User struct {
	Email     string   `json:"email" dynamodbav:"email"`
	Token     string   `json:"token,omitempty" dynamodbav:"-"`
	Username  string   `json:"username" dynamodbav:"username"`
	Bio       string   `json:"bio,omitempty" dynamodbav:"bio,omitempty"`
	Image     string   `json:"image,omitempty" dynamodbav:"image,omitempty"`
	Password  string   `json:"password,omitempty" dynamodbav:"password,omitempty"`
	Following []string `json:"following,omitempty" dynamodbav:"following,omitemptyelem,stringset"`
}

type Profile struct {
	Username  string `json:"username"`
	Bio       string `json:"bio"`
	Image     string `json:"image"`
	Following bool   `json:"following"`
}

type userDBClient interface {
	searchByUsername(username string) (*User, error)
	searchByEmail(email string) (*User, error)
	writeUser(user User, password string) error
	getProfileByUsername(username string, authenticatedUser *User) (*Profile, error)
}

type dynamoDBUserClient struct {
	dynamodb *dynamodb.DynamoDB
	ctx      context.Context
}

type UserReader struct {
	client userDBClient
	user   User
}

type UserWriter struct {
	client userDBClient
	user   User
}

type ProfileReader struct {
	client  userDBClient
	profile Profile
}

type UserClaims struct {
	Username string `json:"Username"`
	jwt.StandardClaims
}
