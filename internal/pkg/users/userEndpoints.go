package users

import (
	"RealWorld/internal/pkg/utils"
	"context"
	"encoding/json"
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/sirupsen/logrus"
	"github.com/tidwall/gjson"
	"go.opencensus.io/trace"
	"sort"
	"strings"
)

func Create(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.Create")
	defer span.End()
	userResult := gjson.Get(event.Body, "user")
	if !userResult.Exists() {
		return utils.Envelope(ctx, 422, "User must be specified.")
	}

	logrus.WithField("UserResult", userResult.String()).Info("Got User")
	userString := strings.Replace(userResult.String(), `\`, "", -1) //For some reason the clients are sending the json doubly escaped, we unescape it here.
	logrus.WithField("unescapedString", userString).Info("Got User")
	user := User{}
	_ = json.Unmarshal([]byte(userString), &user)

	if user.Email == "" {
		return utils.Envelope(ctx, 422, "email must be specified.")
	}

	if user.Username == "" {
		return utils.Envelope(ctx, 422, "username must be specified.")
	}

	if user.Password == "" {
		return utils.Envelope(ctx, 422, "password must be specified")
	}

	reader := UserReader{}
	userWithUserName, _ := reader.getByUsername(ctx, user.Username)

	if userWithUserName.Username != "" {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Username already taken: [%s]", user.Username))
	}

	userWithEmail, _ := reader.getByEmail(ctx, user.Email)
	if userWithEmail != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Email already taken: [%s]", user.Email))
	}

	encryptedPassword, err := hashPassword(user.Password)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("User_String", UserMarshaller(user)),
		}, "Failed to encrypt Password")
		logrus.WithFields(logrus.Fields{
			"Context": ctx,
			"Event":   event,
			"error":   err.Error(),
			"user":    user,
		}).Fatal("Failed to encrypt Password")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	writer := UserWriter{}
	err = writer.writeUser(ctx, User{
		Username: user.Username,
		Email:    user.Email,
		Password: encryptedPassword,
		Image:    "http://placekitten.com/g/200/200",
	}, encryptedPassword)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("User_String", UserMarshaller(user)),
		}, "Failed to Save User")
		logrus.WithFields(logrus.Fields{
			"Context": ctx,
			"Event":   event,
			"error":   err.Error(),
			"user":    user,
		}).Fatal("Failed to Save User")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	token, err := mintUserToken(ctx, user.Username)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"Context": ctx,
			"Event":   event,
			"error":   err.Error(),
			"user":    user,
		}).Fatal("Failed to mint Token")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	return utils.Envelope(ctx, 201, map[string]map[string]string{
		"user": {
			"email":    user.Email,
			"token":    *token,
			"username": user.Username,
			"bio":      "",
			"image":    "http://placekitten.com/g/200/200",
		},
	})
}

func Login(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.Login")
	defer span.End()
	userResult := gjson.Get(event.Body, "user")

	if !userResult.Exists() {
		return utils.Envelope(ctx, 422, "User must be specified.")
	}

	logrus.WithField("UserResult", userResult.String()).Info("Got User")
	userString := strings.Replace(userResult.String(), `\`, "", -1)
	logrus.WithField("unescapedString", userString).Info("Got User")
	user := User{}
	_ = json.Unmarshal([]byte(userString), &user)

	if user.Email == "" {
		return utils.Envelope(ctx, 422, "Email must be specified.")
	}

	if user.Password == "" {
		return utils.Envelope(ctx, 422, "Password must be specified")
	}
	reader := UserReader{}
	userWithEmail, err := reader.getByEmail(ctx, user.Email)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("Email not found: [%s]", user.Email))
	}

	if !checkPasswordHash(user.Password, userWithEmail.Password) {
		return utils.Envelope(ctx, 422, "Wrong Password")
	}
	token, err := mintUserToken(ctx, userWithEmail.Username)
	if err != nil {
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}

	authenticatedUser := userWithEmail
	authenticatedUser.Token = *token
	authenticatedUser.Password = ""
	if authenticatedUser.Bio == "" {
		authenticatedUser.Bio = " "
	}
	return utils.Envelope(ctx, 200, map[string]User{
		"user": *authenticatedUser,
	})
}

func Get(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.Get")
	defer span.End()
	authenticatedUser, err := AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Token not present or invalid.")
	}
	authenticatedUser.Password = ""
	token, _ := getTokenFromEvent(ctx, event)
	authenticatedUser.Token = *token
	if authenticatedUser.Bio == "" {
		authenticatedUser.Bio = " "
	}
	return utils.Envelope(ctx, 200, map[string]User{
		"user": *authenticatedUser,
	})
}

func Update(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.Update")
	defer span.End()
	authenticatedUser, err := AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Token not present or invalid.")
	}

	userResult := gjson.Get(event.Body, "user")
	if !userResult.Exists() {
		return utils.Envelope(ctx, 422, "User must be specified.")
	}

	logrus.WithField("UserResult", userResult.String()).Info("Got User")
	userString := strings.Replace(userResult.String(), `\`, "", -1)
	logrus.WithField("unescapedString", userString).Info("Got User")
	user := User{}
	_ = json.Unmarshal([]byte(userString), &user)

	reader := UserReader{}
	if user.Username != "" {
		authenticatedUser.Username = user.Username
	}
	if user.Email != "" {

		userWithEmail, err := reader.getByEmail(ctx, user.Email)
		if err == nil {
			if authenticatedUser.Email != userWithEmail.Email {
				return utils.Envelope(ctx, 422, fmt.Sprintf("Email already taken: [%s]", user.Email))
			}
			authenticatedUser.Email = user.Email
		}
	}
	if user.Password != "" {
		encryptedPassword, err := hashPassword(user.Password)
		if err != nil {
			span.SetStatus(trace.Status{
				Code:    trace.StatusCodeInternal,
				Message: err.Error(),
			})
			span.Annotate([]trace.Attribute{
				trace.StringAttribute("Event_Body", event.Body),
				trace.StringAttribute("Event_Method", event.HTTPMethod),
				trace.StringAttribute("User_String", UserMarshaller(user)),
			}, "Failed to encrypt Password")
			return utils.Envelope(ctx, 500, "Internal Error with the Server")
		}
		authenticatedUser.Password = encryptedPassword
	}

	writer := UserWriter{}
	err = writer.writeUser(ctx, *authenticatedUser, authenticatedUser.Password)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("Event_Body", event.Body),
			trace.StringAttribute("Event_Method", event.HTTPMethod),
			trace.StringAttribute("User_String", UserMarshaller(user)),
		}, "Failed to encrypt Password")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	token, err := getTokenFromEvent(ctx, event)
	if err != nil {

		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	authenticatedUser.Token = *token
	authenticatedUser.Password = ""
	if authenticatedUser.Bio == "" {
		authenticatedUser.Bio = " "
	}
	return utils.Envelope(ctx, 201, map[string]User{
		"user": *authenticatedUser,
	})
}

func GetProfile(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.Profile")
	defer span.End()
	username, ok := event.PathParameters["username"]
	if !ok || username == "" {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing Username parameter",
		})
		return utils.Envelope(ctx, 403, "username must be specified")
	}
	authenticatedUser, err := AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 403, "Unauthorized User")
	}
	reader := ProfileReader{}
	profile, err := reader.GetProfileByUsername(ctx, username, authenticatedUser)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("User not found: [%s]", username))
	}
	return utils.Envelope(ctx, 200, map[string]Profile{
		"profile": *profile,
	})
}

func Follow(outerCtx context.Context, event events.APIGatewayProxyRequest) (utils.Response, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.Follow")
	defer span.End()
	authenticatedUser, err := AuthenticateAndGetUser(ctx, event)
	if err != nil {
		return utils.Envelope(ctx, 422, "Token not present or invalid.")
	}
	username, ok := event.PathParameters["username"]
	if !ok {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeFailedPrecondition,
			Message: "Missing Username parameter",
		})
	}
	reader := UserReader{}
	user, err := reader.getByUsername(ctx, username)
	if err != nil {
		return utils.Envelope(ctx, 422, fmt.Sprintf("User not found: [%s]", username))
	}
	shouldFollow := event.HTTPMethod != "DELETE"

	if shouldFollow {
		if len(user.Following) > 0 {
			sort.Strings(user.Following)
			i := sort.SearchStrings(user.Following, authenticatedUser.Username)
			if i < len(user.Following) {
				user.Following = append(user.Following, authenticatedUser.Username)
			} else {
				user.Following = []string{authenticatedUser.Username}
			}
		}
	} else {
		if len(user.Following) > 0 {
			sort.Strings(user.Following)
			i := sort.SearchStrings(user.Following, authenticatedUser.Username)
			if i < len(user.Following) {
				user.Following[i] = user.Following[len(user.Following)-1]
				user.Following[len(user.Following)-1] = ""
				user.Following = user.Following[:len(user.Following)-1]
			}
		}
	}
	writer := UserWriter{}
	err = writer.writeUser(ctx, *user, user.Password)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("User_to_follow", UserMarshaller(*user)),
			trace.StringAttribute("User", UserMarshaller(*authenticatedUser)),
		}, "Failed to Update followers field on followed User")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}
	if shouldFollow {
		if len(authenticatedUser.Following) > 0 {
			sort.Strings(authenticatedUser.Following)
			i := sort.SearchStrings(authenticatedUser.Following, username)
			if i < len(user.Following) {
				authenticatedUser.Following = append(authenticatedUser.Following, username)
			} else {
				authenticatedUser.Following = []string{username}
			}
		}
	} else {
		if len(authenticatedUser.Following) > 0 {
			sort.Strings(authenticatedUser.Following)
			i := sort.SearchStrings(authenticatedUser.Following, username)
			if i < len(authenticatedUser.Following) {
				authenticatedUser.Following[i] = authenticatedUser.Following[len(authenticatedUser.Following)-1]
				authenticatedUser.Following[len(authenticatedUser.Following)-1] = ""
				authenticatedUser.Following = authenticatedUser.Following[:len(authenticatedUser.Following)-1]
			}
		}
	}
	err = writer.writeUser(ctx, *user, user.Password)
	if err != nil {
		span.SetStatus(trace.Status{
			Code:    trace.StatusCodeInternal,
			Message: err.Error(),
		})
		span.Annotate([]trace.Attribute{
			trace.StringAttribute("User_to_follow", UserMarshaller(*user)),
			trace.StringAttribute("User", UserMarshaller(*authenticatedUser)),
		}, "Failed to Update followers field on follower User")
		return utils.Envelope(ctx, 500, "Internal Error with the Server")
	}

	profile := Profile{
		Username:  user.Username,
		Bio:       user.Bio,
		Image:     user.Image,
		Following: shouldFollow,
	}

	return utils.Envelope(ctx, 200, map[string]Profile{
		"profile": profile,
	})
}

func GetFollowedUsers(outerCtx context.Context, username string) ([]string, error) {
	ctx, span := trace.StartSpan(outerCtx, "User.GetFollowedUsers")
	defer span.End()
	reader := UserReader{}
	user, err := reader.getByUsername(ctx, username)
	if err != nil {
		return nil, err
	} else {
		return user.Following, nil
	}
}
