package users

import (
	"RealWorld/internal/pkg/dbclient"
	"context"
	"errors"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/sirupsen/logrus"
	"go.opencensus.io/trace"
	"sort"
)

func (reader *UserReader) getByUsername(ctx context.Context, username string) (*User, error) {
	reader.client = &dynamoDBUserClient{nil, ctx}
	user, err := reader.client.searchByUsername(username)
	if err != nil {
		return nil, err
	} else {
		reader.user = *user
		return user, nil
	}
}

func (reader *UserReader) getByEmail(ctx context.Context, email string) (*User, error) {
	reader.client = &dynamoDBUserClient{nil, ctx}
	user, err := reader.client.searchByEmail(email)
	if err != nil {
		return nil, err
	} else {
		reader.user = *user
		return user, nil
	}
}

func (reader *ProfileReader) GetProfileByUsername(ctx context.Context, username string, authenticatedUser *User) (*Profile, error) {
	reader.client = &dynamoDBUserClient{nil, ctx}
	profile, err := reader.client.getProfileByUsername(username, authenticatedUser)
	if err != nil {
		return nil, err
	} else {
		reader.profile = *profile
		return profile, nil
	}
}

func (writer *UserWriter) writeUser(ctx context.Context, user User, password string) error {
	writer.client = &dynamoDBUserClient{nil, ctx}
	err := writer.client.writeUser(user, password)
	if err != nil {
		return err
	} else {
		writer.user = user
		return nil
	}
}

func (client *dynamoDBUserClient) writeUser(user User, password string) error {
	ctx, span := trace.StartSpan(client.ctx, "DbClient.WriteUser")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return err
	} else {
		usersTable, err := dbclient.GetTableName(ctx, "users")
		if err != nil {
			return err
		} else {
			putItemInput := dynamodb.PutItemInput{}
			item, _ := dynamodbattribute.MarshalMap(user)
			putItemInput = *putItemInput.SetTableName(usersTable).SetItem(item)
			_, err := client.dynamodb.PutItem(&putItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Put_Item_Input", putItemInput.String()),
					trace.StringAttribute("User_Name", user.Username),
					trace.StringAttribute("Users_Table", usersTable),
				}, "Failed to Save User")
				return err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Created User",
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Put_Item_Input", putItemInput.String()),
					trace.StringAttribute("User_Name", user.Username),
					trace.StringAttribute("Users_Table", usersTable),
				}, "Successfully Created User")
				return nil
			}
		}
	}
}

func (client *dynamoDBUserClient) searchByUsername(username string) (*User, error) {
	ctx, span := trace.StartSpan(client.ctx, "User.SearchByUsername")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return nil, err
	} else {
		usersTable, err := dbclient.GetTableName(ctx, "users")
		if err != nil {
			return nil, err
		} else {
			getItemInput := dynamodb.GetItemInput{}
			getItemInput = *getItemInput.SetTableName(usersTable).SetKey(map[string]*dynamodb.AttributeValue{
				"username": {
					S: aws.String(username),
				},
			})
			result, err := client.dynamodb.GetItem(&getItemInput)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeNotFound,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("User_Name", username),
					trace.StringAttribute("Users_Table", usersTable),
				}, "Failed to find User")
				return nil, err
			}
			user := User{}
			err = dynamodbattribute.UnmarshalMap(result.Item, &user)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("User_Name", username),
					trace.StringAttribute("Users_Table", usersTable),
				}, "Failed to find User")
				return nil, err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Found User!",
				})

				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Get_Item_Input", getItemInput.String()),
					trace.StringAttribute("User_Name", username),
					trace.StringAttribute("Users_Table", usersTable),
					trace.StringAttribute("User", UserMarshaller(user)),
				}, "Found User!")
				return &user, nil
			}
		}
	}
}

func (client *dynamoDBUserClient) searchByEmail(email string) (*User, error) {
	ctx, span := trace.StartSpan(client.ctx, "User.SearchByEmail")
	defer span.End()
	var err error
	_ = err
	client.dynamodb, err = dbclient.GetDynamoDBClient(ctx)
	if err != nil {
		return nil, err
	} else {
		usersTable, err := dbclient.GetTableName(ctx, "users")
		if err != nil {
			return nil, err
		} else {
			queryInput := dynamodb.QueryInput{}
			query := *queryInput.SetTableName(usersTable).SetIndexName(
				"email").SetKeyConditionExpression(
				"email = :email").SetExpressionAttributeValues(
				map[string]*dynamodb.AttributeValue{
					":email": {
						S: aws.String(email),
					},
				}).SetSelect("ALL_ATTRIBUTES")
			span.Annotate([]trace.Attribute{
				trace.StringAttribute("Query", query.String()),
				trace.StringAttribute("User_Email", email),
				trace.StringAttribute("Users_Table", usersTable),
			}, "Built Query")

			result, err := client.dynamodb.Query(&query)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeNotFound,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", query.String()),
					trace.StringAttribute("User_Email", email),
					trace.StringAttribute("Users_Table", usersTable),
				}, "Failed to find User")
				logrus.WithFields(logrus.Fields{
					"Query":       query.String(),
					"Query_Error": err.Error(),
					"User_Email":  email,
					"Users_Table": usersTable,
				}).Info("Failed to find User")
				return nil, err
			}
			span.Annotate([]trace.Attribute{
				trace.StringAttribute("Query", query.String()),
				trace.StringAttribute("Query_Result", result.String()),
				trace.StringAttribute("User_Email", email),
				trace.StringAttribute("Users_Table", usersTable),
			}, "Got Result")
			logrus.WithFields(logrus.Fields{
				"Query":        query.String(),
				"Query_Result": result.String(),
				"User_Email":   email,
				"Users_Table":  usersTable,
			}).Info("Got Result")
			if len(result.Items) == 0 {
				err = errors.New("could not find user")
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeNotFound,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", query.String()),
					trace.StringAttribute("User_Email", email),
					trace.StringAttribute("Users_Table", usersTable),
				}, "Failed to find User")
				return nil, err
			}
			user := User{}
			err = dynamodbattribute.UnmarshalMap(result.Items[0], &user)
			if err != nil {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeUnknown,
					Message: err.Error(),
				})
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", query.String()),
					trace.StringAttribute("User_Email", email),
					trace.StringAttribute("Users_Table", usersTable),
				}, "Failed to find User")
				return nil, err
			} else {
				span.SetStatus(trace.Status{
					Code:    trace.StatusCodeOK,
					Message: "Found User!",
				})

				span.Annotate([]trace.Attribute{
					trace.StringAttribute("Query", query.String()),
					trace.StringAttribute("User_Email", email),
					trace.StringAttribute("Users_Table", usersTable),
					trace.StringAttribute("User", UserMarshaller(user)),
				}, "Found User!")
				return &user, nil
			}
		}
	}
}

func (client *dynamoDBUserClient) getProfileByUsername(username string, authenticatedUser *User) (*Profile, error) {
	ctx, span := trace.StartSpan(client.ctx, "User.GetProfileByUsername")
	defer span.End()
	reader := UserReader{}
	user, err := reader.getByUsername(ctx, username)

	if err != nil {
		return nil, err
	}
	profile := Profile{
		Username:  user.Username,
		Bio:       user.Bio,
		Image:     user.Image,
		Following: false,
	}

	if len(user.Following) != 0 && authenticatedUser != nil {
		sort.Strings(user.Following)
		i := sort.SearchStrings(user.Following, authenticatedUser.Username)
		profile.Following = i < len(user.Following) && user.Following[i] == authenticatedUser.Username
	}
	return &profile, nil
}
