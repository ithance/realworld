.PHONY: build clean deploy bootstrap teardown testAPI seedXray getWebsiteUrl

SHELL := /bin/bash

toolcheck: checkgit checkpython checkpip checkaws checkgo checkdep checknode
	@echo 'Available tool checking successful, continuing to Codepipeline Cloud Formation deploy'

checkgit:
	@if ! [ -x "$$(command -v git)" ]; then \
          echo 'Error: git is not installed.' >&2;\
          exit 1;\
     fi;

checkgo:
	@if ! [ -x "$$(command -v go)" ]; then \
          echo 'Error: go is not installed.' >&2;\
          exit 1;\
     fi;

checkdep:
	@if ! [ -x "$$(command -v dep)" ]; then \
          echo 'Error: dep for go is not installed.' >&2;\
          exit 1;\
     fi;

checknode:
	@if ! [ -x "$$(command -v npm)" ]; then \
          echo 'Error: nodejs is not installed.' >&2;\
          exit 1;\
     fi;
checkpython:
	@if ! [[ -x "$$(command -v python)" ]]; then \
       echo 'Error: python is not installed.' >&2; \
       exit 1; \
     fi

checkpip:
	@if ! [[ -x "$$(command -v pip)" ]]; then \
       echo 'Error: pip is not installed.' >&2; \
       exit 1; \
     fi

checkaws:
	@if ! [[ -x "$$(command -v aws)" ]]; then \
       pip install awscli; \
       echo "You need to Configure the AWS CLI."; \
       echo "Opening browser..."; \
       open https://docs.aws.amazon.com/rekognition/latest/dg/setting-up.html; \
       aws configure; \
       echo "Thank you!"; \
     fi

validatetemplate:
	@aws cloudformation validate-template --template-body  file://build/Repo-CI-CD.yaml

stackexists: toolcheck validatetemplate
	@if ! aws cloudformation describe-stacks --stack-name RealWorld-API &>/dev/null; then \
        echo 'Setting up Codepipeline'; \
        aws cloudformation create-stack  --stack-name RealWorld-API --capabilities CAPABILITY_NAMED_IAM  --template-body file://build/Repo-CI-CD.yaml; \
        echo 'Waiting for completion...'; \
        aws cloudformation wait stack-create-complete --stack-name RealWorld-API; \
    else \
        echo "Stack already deployed"; \
    fi

setupsshuser: stackexists
	@AWS_USER=$$(aws sts get-caller-identity --query Arn --output text | cut -d '/' -f 2); \
	if [ $$(aws iam list-ssh-public-keys --user-name $$AWS_USER --query SSHPublicKeys --output text | wc -l) == "       0" ]; then \
         if ! [ -e ~/.ssh/id_rsa.pub ]; then \
         	echo "You need to create a default ssh key"; \
            ssh-keygen; \
         fi; \
         echo "Adding default ssh public key(id_rsa.pub) to your IAM user: $$AWS_USER"; \
         aws iam upload-ssh-public-key --user-name $$AWS_USER --ssh-public-key-body file://~/.ssh/id_rsa.pub; \
     else \
         echo "Default ssh public key already added to IAM User: $$AWS_USER"; \
     fi; \
     echo "If you wish to add your preferred key later, run aws iam upload-ssh-public-key help";

bootstrap: setupsshuser
	@echo "Codepipeline Cloud Formation configration step complete, continuing to configure remote aws git repo"
	@AWS_GITREPO=$$(aws cloudformation describe-stacks --stack-name RealWorld-API --query 'Stacks[0].Outputs[?OutputKey==`CodeRepo`].OutputValue' --output text); \
	AWS_USER=$$(aws sts get-caller-identity --query Arn --output text | cut -d '/' -f 2); \
	SSHKeyID=$$(aws iam list-ssh-public-keys --user-name $$AWS_USER --query SSHPublicKeys --output text | cut -f 1); \
	SSH_URL=$$(sed -e "s,ssh://,ssh://$$SSHKeyID@,g" <<< $$AWS_GITREPO); \
	if ! git config remote.aws.url > /dev/null; then \
		 echo "Setting new git remote aws to $$SSH_URL"; \
		 git remote add aws $$AWS_GITREPO; \
    elif git config remote.aws.url != $$SSH_URL; then \
		 echo "Updating git remote aws to $$SSH_URL"; \
		 git remote remove aws; \
		 git remote add aws $$SSH_URL; \
    else \
		 echo "Git remote already setup"; \
    fi

build: checkgo checkdep
	dep ensure -v
	env GOOS=linux go build -ldflags="-s -w" -o bin/createUser cmd/user/create/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/followUser cmd/user/follow/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/loginUser cmd/user/login/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getUser cmd/user/get/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getProfile cmd/user/profile/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/updateUser cmd/user/update/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/createArticle cmd/article/create/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getArticle cmd/article/get/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/updateArticle cmd/article/update/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/deleteArticle cmd/article/delete/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getTags cmd/article/tags/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/listArticles cmd/article/list/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/favoriteArticles cmd/article/favorite/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/articleFeed cmd/article/feed/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/createComment cmd/comment/create/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/getComments cmd/comment/get/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/deleteComment cmd/comment/delete/main.go

clean:
	rm -rf ./bin ./vendor Gopkg.lock website

deploy: checknode checkaws clean build
	sls deploy --verbose
	git clone https://github.com/gothinkster/react-redux-realworld-example-app.git web
	SERVICE_URL=$$(sls info -v | grep ServiceEndpoint | cut -d' ' -f 2); \
	sed -i -e "s,https://conduit.productionready.io/api,$$SERVICE_URL/api,g" web/src/agent.js; \
	cd web; \
	npm install; \
	npm run-script build; \
	cd ../; \
	aws s3 sync web/build/ s3://$$(sls info -v | grep WebsiteBucket | cut -d" " -f 2)

teardown: stackexists
	@echo "Tearing down Stack"
	@-git remote remove aws
	@-S3Bucket=$$(aws cloudformation describe-stacks --stack-name RealWorld-API --query 'Stacks[0].Outputs[?OutputKey==`S3Bucket`].OutputValue' --output text); \
	aws s3 rb s3://$$S3Bucket --force; \
	aws s3 rb s3://$$(sls info -v | grep WebsiteBucket | cut -d" " -f 2) --force
	@-sls remove --verbose
	@aws cloudformation delete-stack --stack-name RealWorld-API
	@echo "Waiting for Stack Deletion to complete..."
	@aws cloudformation wait stack-delete-complete --stack-name RealWorld-API
	@echo "Stack Delete Complete"

testAPI:
	@APIURL=$$(sls info -v | grep ServiceEndpoint | cut -d' ' -f 2)/api ./scripts/run-api-tests.sh

seedXray:
	@for ((i=1;i<=100;i++)); \
    do \
       APIURL=$$(sls info -v | grep ServiceEndpoint | cut -d' ' -f 2)/api ./scripts/run-api-tests.sh; \
    done

getWebsiteUrl: checknode
	@sls info -v | grep WebsiteURL | cut -d" " -f 2