# RealWorld Project #1

This codebase was created to demonstrate a fully fledged fullstack application built with **AWS DynamoDB + Lambda** including CRUD operations, authentication, routing, pagination, and more.

It is written in [go](https://golang.org) and tries to follow a [standard layout structure](https://github.com/golang-standards/project-layout)

For the API Spec checkout [RealWorld](https://github.com/gothinkster/realworld/tree/master/api)
## Getting Started

These instructions will get you a copy of the project up and running on your AWS account for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
git 2.19
python 3.6
nodejs 8 
go 1.10
dep 0.5.0
GNU Make 3.81
```

In addition to these tools, you will also need an AWS account. 

### Installing

I have included a makefile that makes this extremely easy. For you windows folks,
I'll leave it as an exercise to create a build script.

To setup the tooling as well as the CI/CD pipeline:

```
make bootstrap
git push aws master
```

After that,

to get the website url

```
make getWebsiteUrl
```

## Running the tests

After you've deployed the app. Wait about 5-6 minutes, then:

```
make testAPI
make seedXray
```

## Built With

* [go](https://golang.org) - Language used
* [dep](https://github.com/tools/godep) - Dependency Management
* [OpenCensus](https://opencensus.io) - Telemetry Library
* [serverless](https://serverless.com) - web framework used
* [logrus](https://github.com/sirupsen/logrus) - structured logging
* [jwt-go](https://github.com/dgrijalva/jwt-go) - JWT library
* [gjson](https://github.com/tidwall/gjson) - JSON library
* [slug](https://github.com/gosimple/slug) - Slug Library
* [uuid](https://github.com/google/uuid) - UUID Library
* [DynamoDB](https://aws.amazon.com/dynamodb/) - Database
* [Xray](https://aws.amazon.com/xray/) - Distributed Tracing

## Contributing

Please send an email to [support@ithance.com](mailto:support@ithance.com)

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Marc Byfield** - *Initial work* - [IThance](https://www.ithance.com)

## License

This project is licensed under the MIT License
